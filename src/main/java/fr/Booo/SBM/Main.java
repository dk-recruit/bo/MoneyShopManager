package fr.Booo.SBM;

import fr.Booo.SBM.commands.*;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Main extends JavaPlugin {
  FileManager fm = new FileManager(this);
  Chrono chrono = new Chrono(this);
  public Map<UUID, Integer> money = new HashMap<>();

  @Override
  public void onEnable() {
    getCommand("mShop").setExecutor(new ShopManager(this));
    getCommand("tBoost").setExecutor(new BoostCmd(this));

    chrono.runTaskTimer(this, 0, 20);

    saveDefaultConfig();

    fm.recupStat();
  }

  @Override
  public void onDisable() {
    fm.defineStat();
  }
}
