package fr.Booo.SBM;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class Chrono extends BukkitRunnable {
  private Main main;

  public Chrono(Main main) {
    this.main = main;
  }

  @Override
  public void run(){
    new HashMap<>(main.money);

    for (Entry<UUID , Integer> ent : new HashMap<>(main.money).entrySet()) {
      OfflinePlayer offp = Bukkit.getOfflinePlayer(ent.getKey());
      int time = ent.getValue();

      if (time == 0) {
        if (offp.isOnline()) {
          Player p = Bukkit.getPlayer(ent.getKey());
          p.sendMessage(main.getConfig().getString("msg.finish").replace("&", "§"));
        }

        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "shop resetmodifier global " + offp.getName());
        main.money.remove(offp.getUniqueId()) ;
      }

      if (offp.isOnline()) {
        Player p = Bukkit.getPlayer(ent.getKey());

        if (time == 1801) p.sendMessage(main.getConfig().getString("msg.trenteMin").replace("&", "§"));
        if (time == 901) p.sendMessage(main.getConfig().getString("msg.quinzeMin").replace("&", "§"));
        if (time == 601) p.sendMessage(main.getConfig().getString("msg.dixMin").replace("&", "§"));
        if (time == 301) p.sendMessage(main.getConfig().getString("msg.cinqMin").replace("&", "§"));
        if (time == 120) p.sendMessage(main.getConfig().getString("msg.deuxMin").replace("&", "§"));
        if (time == 60) p.sendMessage(main.getConfig().getString("msg.unMin").replace("&", "§"));
        if (time == 30) p.sendMessage(main.getConfig().getString("msg.trenteSec").replace("&", "§"));
      }

      addlist(offp , time);
    }
  }

  public void addlist(OfflinePlayer p , int time) {
    main.money.remove(p.getUniqueId()) ;
    main.money.put(p.getUniqueId() , time - 1);
  }
}
