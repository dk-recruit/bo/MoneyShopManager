package fr.Booo.SBM.commands;

import fr.Booo.SBM.Main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BoostCmd implements CommandExecutor {
  private Main main;

  public BoostCmd(Main main) {
    this.main = main;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (sender instanceof Player) {
      Player player = (Player) sender;

      if (main.money.containsKey(player.getUniqueId())) {
        String msg = main.getConfig().getString("PlayerMsg").replace("%BoostTime%", main.money.get(player.getUniqueId()).intValue() + "").replace("&", "§");
        player.sendMessage(msg);
      } else {
        String msg = main.getConfig().getString("ErrorBoost").replace("&", "§");
        player.sendMessage(msg);
      }
    }

    return false;
  }
}
