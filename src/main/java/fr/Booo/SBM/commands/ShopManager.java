package fr.Booo.SBM.commands;

import fr.Booo.SBM.Main;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map.Entry;
import java.util.UUID;

public class ShopManager implements CommandExecutor {
  private Main main;

  public ShopManager(Main main) {
    this.main = main;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
    if (sender instanceof Player && args.length == 0) {
      MpPlayer((Player) sender);
    }

    if (args.length >= 1) {
      switch (args[0]) {
        case "add":
          if (args.length == 3) {
            int time;
            Player cible = Bukkit.getPlayer(args[1]);

            try {
              time = Integer.parseInt(args[2]);

              if (main.money.containsKey(cible.getUniqueId())) {
                int newTime = main.money.get(cible.getUniqueId()).intValue() + time * 60;
                main.money.put(cible.getUniqueId(), newTime );
                cible.sendMessage(main.getConfig().getString("Use.AdditionMsg").replace("&", "§").replace("%oldBoost%", main.money.get(cible.getUniqueId()).intValue() + "").replace("%newBoost", newTime + "").replace("%addedBoost%", time + ""));
              } else {
                main.money.put(cible.getUniqueId(), time * 60);
              }

              if (sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage("§8[§6§lBoooFo§8] §2Le joueur : §6" + cible.getName() + "§2 A obtenue un boost de :§6 " + time + " §2Minute de +");
              }

              Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "/shop addmodifier global " + cible.getName() + " 2 sell");
            } catch(ArithmeticException e) {
              if(sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage("§8[§6§lBoooFo§8] §2Tu as retiré le Boost du joueur : §6" + p.getName());
              }
            }
          }
          break;

        case "remove":
          if (args.length == 2) {
            Player cible = Bukkit.getPlayer(args[1]);

            if (main.money.containsKey(cible.getUniqueId())) {
              main.money.remove(cible.getUniqueId());

              if (sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage("§8[§6§lBoooFo§8] §2Tu as retiré le Boost du joueur : §6" + p.getName());
              }
            } else if(sender instanceof Player) {
              Player p = (Player) sender;
              p.sendMessage("§8[§6§lBoooFo§8] §4Erreur: §8Le joueur n'est pas Booster...");
            }
          } else if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage("§8[§6§lBoooFo§8] §4Erreur: §8 Faites : /Mshop remove <Player>");
          }
          break;

        case "check":
          if (args.length == 2) {
            Player cible = Bukkit.getPlayer(args[1]);
            Player player = (Player) sender;

            if (main.money.containsKey(cible.getUniqueId())) {
              player.sendMessage("§8[§6§lBoooFo§8] §2Player : §6" + cible.getName() + "§2 Boosted : §6x2 §2, Time: §6" + main.money.get(cible.getUniqueId()).intValue() + "§2 secondes");
            } else {
              player.sendMessage("§8[§6§lBoooFo§8] §4Erreur: §8Le joueur n'est pas Booster...");
            }
          } else if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage("§8[§6§lBoooFo§8] §4Erreur: §8 Faites : /Mshop check <Player>");
          }
          break;

        case "list":
          ListBoosted(sender);
          break;

        default :
          MpPlayer((Player) sender);
          break;
      }
    }

    return false;
  }

  public void MpPlayer(Player p) {
    p.sendMessage("§7--------§8[§9Booo §8- §2Shop§8]§7---------");
    p.sendMessage("§7Ajouter a la liste : §2/Mshop add <Player> <Time>");
    p.sendMessage("§7Retirer de la liste : §2/Mshop remove <Player>");
    p.sendMessage("§7Check le joueur de la liste : §2/Mshop check <Player>");
    p.sendMessage("§7avoir la liste des joueurs : §2/Mshop list");
    p.sendMessage("§7-----------------------------");
  }

  public void ListBoosted(CommandSender sender) {
    if (sender instanceof Player) {
      Player p = (Player) sender;
      p.sendMessage("§8--------[§9Booo §8- §2List§8]---------");

      for (Entry<UUID, Integer> ent : main.money.entrySet()) {
        OfflinePlayer cible = Bukkit.getOfflinePlayer(ent.getKey());
        int time = ent.getValue();

        p.sendMessage("§8 - §6" + cible.getName() + "§7 (Time : §2" + time + " §7)");
      }
    }
  }
}
