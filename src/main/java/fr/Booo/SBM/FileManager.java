package fr.Booo.SBM;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.Map.Entry;

public class FileManager {
  private Main main;

  public FileManager(Main main) {
    this.main = main;
  }

  public void recupStat() {
    final File file = new File(main.getDataFolder(), "AllTime.yml");
    final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

    if (!file.exists()) return;

    for (String part : config.getConfigurationSection("time").getKeys(false)) {
      String save = config.getConfigurationSection("time").getString(part + ".nbr");
      String[] saveL = save.split("\\.");

      main.money.put(UUID.fromString(saveL[0]), Integer.parseInt(saveL[1]));
    }

    file.delete();
  }

  public void defineStat() {
    for (Entry<UUID, Integer> ent : main.money.entrySet()) {
      final UUID uuid = ent.getKey();

      final File file = new File(main.getDataFolder(), "AllTime.yml");
      final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      final String stock = uuid.toString() + "." + ent.getValue();

      if (file.exists()) file.delete();

      config.set("time." + Bukkit.getOfflinePlayer(uuid).getName() +".nbr" , stock);

      try {
        config.save(file);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
